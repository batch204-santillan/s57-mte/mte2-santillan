let collection = [];

// Write the queue functions below.

/* WARNING:

    Usage of JS Array Methods (push, pop, shift, unshift, etc)
    is not allowed

*/

// PRINT
    /*
    outputall the elements of the queue
    */
    function print() {
        return collection
    }

// ENQUEUE
    /*
    add element to the rear of the queue
    */
    function enqueue(element) {
       
        collection[collection.length] = element;
        return collection
    }

// DEQUEUE
    /*
    remove element at the FRONT of the queue
    */
    function dequeue() {
        for (let i = 0; i < collection.length; i++)
        {
            collection[i] = collection[i+1]
        }
        collection.length = collection.length-1
        return collection
    }

// FRONT
    /*
    show element at the FRONT
    */
    function front() {
        
        return collection[0]
    }

// SIZE
    /*
    show TOTAL number of elements
    */
    function size() {
        return collection.length
    }

// isEmpty
    /*
    outputs a Boolean value desrcibing wheter the queue is empty or not
    */
    function isEmpty() {
        if (collection.length !== 0)
        {
            return false
        }
        else
            {return true}
    }


// EXPORT MODULES
module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};